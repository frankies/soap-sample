package org.frankies.axis.listener;

import java.net.URL;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import org.frankies.axis.client.stub.MyService;
import org.frankies.axis.client.stub.MyServiceService;

/**
 * Application Lifecycle Listener implementation class AxisClientListener
 *
 */
public class AxisClientListener implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent sce) {
        System.out.println("Start");
        printSource("org.jboss.ws.core.soap.SOAPMessageImpl");
//        printSource("javax.xml.soap.SOAPMessage");
        MyServiceService sis = new MyServiceService();
        MyService si = sis.getMyServicePort();
        System.out.println(si.getServerName());
    }

    @Override
    public void contextDestroyed(ServletContextEvent sce) {
        System.out.println("Destroy");
    }
    
    private void printSource(String clzz)  {
        Class c = null;
        try {
            c = Class.forName(clzz);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException("xx", e);
        }
        URL u = c.getProtectionDomain().getCodeSource().getLocation();
        System.out.println(String.format("%s:%s", c, u));
    }
 
	
}
