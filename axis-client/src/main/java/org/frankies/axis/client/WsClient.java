package org.frankies.axis.client;

import org.frankies.axis.client.stub.MyService;
import org.frankies.axis.client.stub.MyServiceService;

/**
 * Hello world!
 *
 */
public class WsClient 
{
    public static void main( String[] args )
    {
        MyServiceService sis = new MyServiceService();
        MyService si = sis.getMyServicePort();

        System.out.println(si.getServerName());
    }
}
