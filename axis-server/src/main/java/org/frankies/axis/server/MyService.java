package org.frankies.axis.server;

import javax.jws.HandlerChain;
import javax.jws.WebMethod;
import javax.jws.WebService;

/**
 *
 * @author YMSLX
 * @version 1.0
 *
 */
@WebService
//@HandlerChain(file="handler-chain.xml")
public class MyService{

    @WebMethod
    public String getServerName() {
        return "Hello world!!";
    }

}
