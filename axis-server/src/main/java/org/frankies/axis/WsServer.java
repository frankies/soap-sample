package org.frankies.axis;

import javax.xml.ws.Endpoint;

import org.frankies.axis.server.MyService;

/**
 * Hello world!
 *
 */
public class WsServer {
    public static void main(String[] args) {
        Endpoint.publish("http://localhost:8888/ws/server", new MyService());
        System.out.println("Service is published!");
        System.out.println("You can access 'http://localhost:8888/ws/server?wsdl'..");
    }
}
